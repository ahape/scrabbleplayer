﻿using System;
using System.Collections.Generic;

namespace PlayTicTacToe
{
    public class TicTacToeState
    {
        public TicTacToeState(int playerToMove, string squares)
        {
            PlayerToMove = playerToMove;
            Squares = squares;
        }

        public int PlayerToMove { get; }
        public string Squares { get; }

        public bool IsTerminal =>
            !Squares.Contains(" ") ||
            (Squares[0] != ' ' && Squares[0] == Squares[1] && Squares[1] == Squares[2]) ||
            (Squares[3] != ' ' && Squares[3] == Squares[4] && Squares[4] == Squares[5]) ||
            (Squares[6] != ' ' && Squares[6] == Squares[7] && Squares[7] == Squares[8]) ||
            (Squares[0] != ' ' && Squares[0] == Squares[3] && Squares[3] == Squares[6]) ||
            (Squares[1] != ' ' && Squares[1] == Squares[4] && Squares[4] == Squares[7]) ||
            (Squares[2] != ' ' && Squares[2] == Squares[5] && Squares[5] == Squares[8]) ||
            (Squares[0] != ' ' && Squares[0] == Squares[4] && Squares[4] == Squares[8]) ||
            (Squares[2] != ' ' && Squares[2] == Squares[4] && Squares[4] == Squares[6]);

        public double[] Utility()
        {
            if (IsWinFor('X'))
            {
                return new double[] { 1, -1 };
            }
            if (IsWinFor('O'))
            {
                return new double[] { -1, 1 };
            }
            return new double[] { 0, 0 };
        }

        private bool IsWinFor(char mark)
        {
            return
                (Squares[0] == mark && Squares[1] == mark && Squares[2] == mark) ||
                (Squares[3] == mark && Squares[4] == mark && Squares[5] == mark) ||
                (Squares[6] == mark && Squares[7] == mark && Squares[8] == mark) ||
                (Squares[0] == mark && Squares[3] == mark && Squares[6] == mark) ||
                (Squares[1] == mark && Squares[4] == mark && Squares[7] == mark) ||
                (Squares[2] == mark && Squares[5] == mark && Squares[8] == mark) ||
                (Squares[0] == mark && Squares[4] == mark && Squares[8] == mark) ||
                (Squares[2] == mark && Squares[4] == mark && Squares[6] == mark);
        }

        public override string ToString()
        {
            return "\n" + Squares.Substring(0, 3)
                + "\n" + Squares.Substring(3, 3)
                + "\n" + Squares.Substring(6, 3)
                + "\n";
        }
    }
}
