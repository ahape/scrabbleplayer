﻿using GamePlayerNs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlayTicTacToe
{
    class Program
    {
        static void TestMinimax()
        {
            var mm = Minimax.Create(new TicTacToeGame());
            var state = new TicTacToeState(0, "         ");
            while (!state.IsTerminal)
            {
                var best = mm.Search(state, CancellationToken.None);
                if (best == null)
                    break;
                Console.WriteLine(best);
                Console.WriteLine(mm.GetStats());
                state = best.State;
            }
        }

        static void TestExpectiminimax()
        {
            var emm = Expectiminimax.Create(new TicTacToeGame());
            var state = new TicTacToeState(0, "         ");
            while (!state.IsTerminal)
            {
                var best = emm.Search(state, CancellationToken.None);
                if (best == null)
                    break;
                Console.WriteLine(best);
                Console.WriteLine(emm.GetStats());
                state = best.State;
            }
        }

        static void Main(string[] args)
        {
            if (args.Length == 0 || args[0] == "minimax")
                TestMinimax();
            else if (args[0] == "expectiminimax")
                TestExpectiminimax();
            else
                Console.WriteLine("Unknown");
            Console.ReadLine();
        }
    }
}
