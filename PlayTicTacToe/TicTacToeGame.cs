﻿using GamePlayerNs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTicTacToe
{
    class TicTacToeGame : IChanceGameRules<TicTacToeState, TicTacToeMove>
    {
        public IEnumerable<Move<TicTacToeState, TicTacToeMove>> Expand(TicTacToeState state)
        {
            for (int i = 0; i < 9; i++)
            {
                if (state.Squares[i] == ' ')
                {
                    var newSquares = state.Squares.ToCharArray();
                    newSquares[i] = "XO"[state.PlayerToMove];
                    yield return Move.Create(
                        new TicTacToeState((state.PlayerToMove + 1) % 2, new string(newSquares)),
                        new TicTacToeMove(i, newSquares[i]));
                }
            }
        }

        public IEnumerable<Chance<TicTacToeState>> ExpandChances(TicTacToeState state)
        {
            return new[] { new Chance<TicTacToeState>(state, 1, 1) };
        }

        public bool IsTerminal(TicTacToeState state)
        {
            return state.IsTerminal;
        }

        public int PlayerToMove(TicTacToeState state)
        {
            return state.PlayerToMove;
        }

        public double[] Utility(TicTacToeState state)
        {
            return state.Utility();
        }

        public double[] Heuristic(TicTacToeState state)
        {
            return new double[] { 0, 0 };
        }
    }
}
