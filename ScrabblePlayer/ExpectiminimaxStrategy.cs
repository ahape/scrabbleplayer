﻿using GamePlayerNs;
using ScrabbleMoveGenerator;
using System;
using System.Threading;

namespace ScrabblePlayer
{
    public class ExpectiminimaxStrategy : IStrategy
    {
        public PotentialMove ChooseMove(GameState state, IWordDatabase wordlist)
        {
            var emm = Expectiminimax.Create(new GameRules(wordlist));
            var cts = new CancellationTokenSource(TimeSpan.FromMinutes(1));
            var move = emm.Search(state, cts.Token);
            return move?.Action;
        }
    }
}
