﻿using GamePlayerNs;
using ScrabbleMoveGenerator;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace ScrabblePlayer
{
    public class GameRules : IChanceGameRules<GameState, PotentialMove>
    {
        private readonly IWordDatabase wordlist;

        public GameRules(IWordDatabase wordlist)
        {
            this.wordlist = wordlist;
        }

        public IEnumerable<Move<GameState, PotentialMove>> Expand(GameState state)
        {
            var gen = new MoveGenerator(state.Board, wordlist);
            var moves = gen.FindMoves(state.CurrentPlayer.Tiles.ToCharArray());
            return moves.Select(move =>
                new Move<GameState, PotentialMove>(state.Play(move), move));
        }

        struct Occurrence
        {
            public GameState State { get; set; }
            public int Count { get; set; }
        }

        public IEnumerable<Chance<GameState>> ExpandChances(GameState state)
        {
            if (state.Bag.IsEmpty)
                return new[] { new Chance<GameState>(state, Ratio.One) };

            // try each combination of remaining tiles from bag on open tile slots
            var remainingTiles = state.Bag;
            int total = 0;
            var chances = new Dictionary<string, Occurrence>();
            var player = state.CurrentPlayer;
            foreach (var (hand, bag) in PossibleHands(player.Tiles, remainingTiles))
            {
                total++;
                if (chances.TryGetValue(hand, out var occurence))
                {
                    occurence.Count++;
                }
                else
                {
                    occurence = new Occurrence
                    {
                        State = state.WithDraw(hand, bag),
                        Count = 1,
                    };
                    chances[hand] = occurence;
                }
            }

            return chances.Values.Select(o => new Chance<GameState>(o.State, o.Count, total));
        }

        private IEnumerable<(string, ImmutableList<char>)> PossibleHands(string tiles, ImmutableList<char> remainingTiles)
        {
            var hands = new List<(string, ImmutableList<char>)>();
            BuildPossibleHands(hands, tiles, remainingTiles, 0);
            return hands;
        }

        private void BuildPossibleHands(List<(string, ImmutableList<char>)> hands, string currentHand, ImmutableList<char> remainingTiles, int from)
        {
            if (currentHand.Length == 7 || from >= remainingTiles.Count)
            {
                hands.Add((currentHand, remainingTiles));
            }
            else
            {
                for (int i = 0; i < remainingTiles.Count; i++)
                {
                    BuildPossibleHands(hands, currentHand + remainingTiles[i], remainingTiles.RemoveAt(i), i);
                }
            }
        }

        public double[] Heuristic(GameState state)
        {
            return Utility(state);
        }

        public bool IsTerminal(GameState state)
        {
            return state.Bag.IsEmpty && state.Players.Any(p => string.IsNullOrEmpty(p.Tiles));
        }

        public int PlayerToMove(GameState state)
        {
            return state.CurrentPlayerIndex;
        }

        public double[] Utility(GameState state)
        {
            var scores = new int[state.Players.Count];
            for (int i = 0; i < scores.Length; i++)
            {
                scores[i] = state.Players[i].Score;
            }
            var utility = new double[scores.Length];
            for (int i = 0; i < utility.Length; i++)
            {
                var highestAmongOthers = 0;
                for (int j = 0; j < utility.Length; j++)
                {
                    if (i != j)
                    {
                        var otherScore = scores[j];
                        if (otherScore > highestAmongOthers)
                        {
                            highestAmongOthers = otherScore;
                        }
                    }
                }
                utility[i] = scores[i] - highestAmongOthers;
            }
            return utility;
        }
    }
}
