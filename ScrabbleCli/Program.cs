﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ScrabbleMoveGenerator;
using ScrabblePlayer;
using ScrabbleMoveApi;

namespace ScrabbleCli
{
    static class Program
    {
        static void TestWordList(IWordDatabase wordlist)
        {
            var word = Console.ReadLine().Trim();
            while (!string.IsNullOrEmpty(word))
            {
                if (word.EndsWith("+"))
                    Console.WriteLine(wordlist.ContainsPrefix(word.TrimEnd('+')));
                else
                    Console.WriteLine(wordlist.Contains(word));
                word = Console.ReadLine().Trim();
            }
        }

        static void FindBestWord(string[] args)
        {
            var wordlist = new NaspaWordList();
            var state = PrepareGameState(args);
            Console.WriteLine(state.Board);
            var tiles = LoadTiles(args);
            var sw = new Stopwatch();
            sw.Start();
            var gen = new MoveGenerator(state.Board, wordlist);
            var moves = gen.FindMoves(tiles);
            sw.Stop();
            Console.WriteLine("Found {0} moves in {1}", moves.Count, sw.Elapsed);
            foreach (var move in moves.Take(20))
            {
                Console.WriteLine(move);
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine();
                Console.WriteLine("findbest TILES [import <filename>]");
                Console.WriteLine("  Find the best word to use given TILES.  Will use an empty initial board,");
                Console.WriteLine("  or with `import` a current board state downloaded from ScrabbleBot");
            }
            else if (args[0] == "findbest")
            {
                FindBestWord(args);
            }
            else if (args[0] == "wordlist")
            {
                TestWordList(new NaspaWordList());
            }
            else if (args[0] == "autogame")
            {
                int seed;
                if (args.Length > 2)
                    seed = int.Parse(args[2]);
                else
                    seed = new Random().Next();
                PlayAutoGame(args[1].Split(','), seed);
            }
            else if (args[0] == "api")
            {
                switch (args[1]) {
                    case "wordcheck":
                        var verdict = Api.IsWordReal(args[2]);
                        
                        Console.WriteLine("Is the word real? {0}", verdict);
                        break;
                    default:
                        var teams = 2;
                        var encBoard = "".ToCharArray();
                        var bag = "A".ToCharArray();
                        var rack = "ABCDEF".ToCharArray();
                        var teamTurn = 0;
                        var bestWord = Api.FindBestWord(teams, encBoard, bag, rack, teamTurn);

                        Console.WriteLine("Best word is " + bestWord.TotalScore); // Should be "FACED" (30)
                        break;
                }
            }
        }

        static IList<char> LoadTiles(string[] args)
        {
            return args[1].ToCharArray();
        }

        static GameState PrepareGameState(string[] args)
        {
            if (args.Length > 2)
            {
                var arg = args[2];
                if (arg == "import")
                {
                    return ImportScrabbleBot(args[3]);
                }
            }
            var players = new[] { "Team 1", "Team 2" };
            var state = GameState.Create(1, players);
            return state;
        }

        static GameState ImportScrabbleBot(string path)
        {
            var doc = System.Text.Json.JsonDocument.Parse(System.IO.File.ReadAllText(path));
            var teams = doc.RootElement.GetProperty("teams").EnumerateArray().Select(e => e.GetString()).ToList();
            var boardArray = doc.RootElement.GetProperty("board");
            var squares = boardArray.EnumerateArray().Select(square => StringToTile(square.GetString())).ToArray();
            return new GameState(new Board(squares), "", teams);
        }

        static char StringToTile(string str)
        {
            if (string.IsNullOrEmpty(str)) return default;
            return str[0];
        }

        static void PlayAutoGame(IList<string> players, int seed = 0)
        {
            var wordlist = new RankedWordList(
                threshold: 20000,
                includeTwoLetterWords: true,
                includeUlessQWords: true);
            var gamestate = GameState.Create(seed, players);
            var sw = new Stopwatch();
            var strategies = players.Select(CreateInstance).ToList();
            var consecutiveSkips = 0;
            while (GameNotFinished(gamestate) && consecutiveSkips != players.Count)
            {
                gamestate = gamestate.Draw();
                var strategy = strategies[gamestate.CurrentPlayerIndex];
                sw.Restart();
                var bestMove = strategy.ChooseMove(gamestate, wordlist);
                Console.WriteLine(sw.Elapsed);
                if (bestMove == null)
                {
                    gamestate = gamestate.Skip();
                    consecutiveSkips++;
                }
                else
                {
                    gamestate = gamestate.Play(bestMove);
                    consecutiveSkips = 0;
                    Console.WriteLine(bestMove);
                }
                PrintGameState(gamestate);
            }
            if (!gamestate.Bag.IsEmpty)
            {
                Console.WriteLine("Bag tiles left: {0}", new string(gamestate.Bag.ToArray()));
            }
            foreach (var player in gamestate.Players)
            {
                Console.WriteLine("Player {0} tiles left: {1}", player.Name, player.Tiles);
            }
        }

        static IStrategy CreateInstance(string strategyName)
        {
            foreach (var ass in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var typ in ass.GetTypes())
                {
                    if (typ.Name == strategyName && typeof(IStrategy).IsAssignableFrom(typ))
                    {
                        return (IStrategy)Activator.CreateInstance(typ);
                    }
                }
            }
            throw new ArgumentException("No matching strategy found", nameof(strategyName));
        }

        static void PrintGameState(GameState gameState)
        {
            Console.WriteLine("-------------");
            Console.WriteLine(gameState.Board);
            foreach (var player in gameState.Players)
            {
                Console.WriteLine("{0}: {1}", player.Name, player.Score);
            }
        }

        static bool GameNotFinished(GameState state)
        {
            return state.Bag.Any() || state.Players.All(p => p.Tiles.Length > 0);
        }
    }
}
