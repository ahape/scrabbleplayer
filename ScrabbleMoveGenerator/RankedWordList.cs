﻿using System;
using System.IO;

namespace ScrabbleMoveGenerator
{
    /// <summary>
    /// The valid words from the NASPA word list with rankings by Google Ngram API.
    /// </summary>
    public class RankedWordList : IndexedWordList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="threshold">Minimum threshold to allow.  A lower value will result in more obscure words being used.  A higher value will only allow more common words.  A reasonable value might be 20000.</param>
        /// <param name="includeTwoLetterWords">Include all two letter words, regardless of rank (as if the player has a list of valid two-letter words).</param>
        /// <param name="includeUlessQWords">Include all Q words without a U (as if the player has a list of valid U-less Q words).</param>
        public RankedWordList(int threshold, bool includeTwoLetterWords, bool includeUlessQWords)
        {
            foreach (var line in File.ReadLines(GetFilePath()))
            {
                var parts = line.Trim().Split(',');
                var word = parts[0];
                var rank = int.Parse(parts[1]);
                if (rank >= threshold ||
                    (includeTwoLetterWords && word.Length == 2) ||
                    (includeUlessQWords && IsUlessQ(word)))
                {
                    Add(word);
                }
            }
        }

        private static bool IsUlessQ(string word)
        {
            return word.Contains("q") && !word.Contains("u");
        }

        private static string GetFilePath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "wordranks.csv");
        }
    }
}
