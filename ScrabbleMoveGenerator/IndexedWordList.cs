﻿namespace ScrabbleMoveGenerator
{
    /// <summary>
    /// Base class for word lists that keep words indexed in memory.
    /// </summary>
    public abstract class IndexedWordList : IWordDatabase
    {
        private readonly WordListNode root = new WordListNode();

        protected void Add(string word)
        {
            root.Add(word.ToUpperInvariant(), 0);
        }

        public bool Contains(string word)
        {
            return FindNode(word)?.Word != null;
        }

        public bool ContainsPrefix(string prefix)
        {
            return FindNode(prefix)?.children != null;
        }

        private WordListNode FindNode(string str)
        {
            var pos = 0;
            var node = root;
            while (node != null && pos < str.Length)
            {
                var idx = str[pos] - 65;
                node = node.children?[idx];
                pos++;
            }
            return node;
        }
    }
}
