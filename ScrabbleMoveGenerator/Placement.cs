﻿using System.Collections.Generic;

namespace ScrabbleMoveGenerator
{
    public class Placement
    {
        public Placement(char tile, int square)
        {
            Tile = tile;
            Square = square;
        }
        public char Tile { get; }
        public int Square { get; }

        public static IList<Placement> BeginList(char tile, int square)
        {
            return new List<Placement>()
            {
                new Placement(tile, square)
            };
        }
    }
}
