﻿namespace ScrabbleMoveGenerator
{
    class WordListNode
    {
        public string Word { get; private set; }
        public WordListNode[] children;

        public void Add(string word, int pos)
        {
            if (children == null)
                children = new WordListNode[26];
            var letter = word[pos];
            var idx = (int)letter - 65;
            var child = children[idx];
            if (child == null)
            {
                child = new WordListNode();
                children[idx] = child;
                
            }
            if (pos == word.Length - 1)
            {
                child.Word = word;
            }
            else
            {
                child.Add(word, pos + 1);
            }
        }
    }
}
