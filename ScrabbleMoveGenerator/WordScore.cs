﻿namespace ScrabbleMoveGenerator
{
    public class WordScore
    {
        public WordScore(string word, int score)
        {
            Word = word;
            Score = score;
        }
        public string Word { get; }
        public int Score { get; }
    }
}
