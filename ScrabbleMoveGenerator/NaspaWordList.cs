﻿using System;
using System.IO;

namespace ScrabbleMoveGenerator
{
    /// <summary>
    /// The complete NASPA word list 2020
    /// </summary>
    public class NaspaWordList : IndexedWordList
    {
        public NaspaWordList()
        {
            foreach (var line in File.ReadLines(GetFilePath()))
            {
                Add(line.Trim());
            }
        }

        private static string GetFilePath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "nwl2020.txt");
        }
    }
}
