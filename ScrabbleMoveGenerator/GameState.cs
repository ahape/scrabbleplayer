﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace ScrabbleMoveGenerator
{
    public class GameState
    {
        public Board Board { get; }
        public ImmutableList<Player> Players { get; }
        public ImmutableList<char> Bag { get; }
        public int CurrentPlayerIndex { get; }
        public Player CurrentPlayer => Players[CurrentPlayerIndex];
        
        /// <summary>
        /// Create a new game state from a board state, tile bag, and player names
        /// </summary>
        /// <param name="board">Current state of the board</param>
        /// <param name="bag">Tiles left in the bag</param>
        /// <param name="players">List of players by name</param>
        public GameState(Board board, string bag, IList<string> players)
        {
            Board = board;
            Bag = bag.ToImmutableList();
            Players = ImmutableList<Player>.Empty;
            for (int i = 0; i < players.Count; i++)
            {
                Players = Players.Add(new Player(players[i], 0, ""));
            }
        }

        public GameState(Board board, ImmutableList<Player> players, ImmutableList<char> bag, int currentPlayerIndex)
        {
            Board = board;
            Players = players;
            Bag = bag;
            CurrentPlayerIndex = currentPlayerIndex;
        }

        /// <summary>
        /// Create a new random game state.
        /// </summary>
        /// <param name="seed"></param>
        /// <param name="players"></param>
        /// <returns></returns>
        public static GameState Create(int seed, IList<string> players)
        {
            var random = new Random(seed);
            var tiles = GameRules.TileDistribution
                .SelectMany(kv => Enumerable.Repeat(kv.Key, kv.Value))
                .OrderBy(_ => random.Next())
                .ToArray();
            return new GameState(new Board(), new string(tiles), players);
        }

        /// <summary>
        /// Advance the game by having the current player draw tiles from the bag.
        /// </summary>
        /// <returns></returns>
        public GameState Draw()
        {
            var tiles = CurrentPlayer.Tiles;
            var bag = Bag;
            if (tiles.Length < 7)
            {
                var toTake = Math.Min(7 - tiles.Length, bag.Count);
                tiles += string.Join("", bag.Take(toTake));
                bag = bag.RemoveRange(0, toTake);
            }
            var player = CurrentPlayer.WithTiles(tiles);
            var players = Players.SetItem(CurrentPlayerIndex, player);
            return new GameState(Board, players, bag, CurrentPlayerIndex);
        }

        /// <summary>
        /// Advance the game by having the current player skip their turn.
        /// </summary>
        /// <returns></returns>
        public GameState Skip()
        {
            return new GameState(Board, Players, Bag, NextPlayerIndex);
        }

        /// <summary>
        /// Advance the game by having the current player play the potential move provided.
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public GameState Play(PotentialMove move)
        {
            var board = Board.ToCharArray();
            var newtiles = CurrentPlayer.Tiles.ToList();
            foreach (var placement in move.Placements)
            {
                board[placement.Square] = placement.Tile;
                newtiles.Remove(char.IsUpper(placement.Tile) ? placement.Tile : '_');
            }
            var player = new Player(
                CurrentPlayer.Name,
                CurrentPlayer.Score + move.TotalScore,
                string.Join("", newtiles));
            return new GameState(new Board(board), Players.SetItem(CurrentPlayerIndex, player), Bag, NextPlayerIndex);
        }

        /// <summary>
        /// Advance the game state by giving the current player the hand provided, with the provided tiles left in the bag.
        /// </summary>
        /// <param name="hand"></param>
        /// <param name="bag"></param>
        /// <returns></returns>
        public GameState WithDraw(string hand, ImmutableList<char> bag)
        {
            return new GameState(
                Board,
                Players.SetItem(CurrentPlayerIndex, CurrentPlayer.WithTiles(hand)),
                bag,
                CurrentPlayerIndex);
        }

        private int NextPlayerIndex => (CurrentPlayerIndex + 1) % Players.Count;
    }
}
