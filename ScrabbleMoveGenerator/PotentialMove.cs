﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrabbleMoveGenerator
{
    public class PotentialMove
    {
        private readonly Board board;
        
        public int TotalScore { get; }

        public IList<WordScore> Words { get; }

        private string stemWordHorizontal;
        private string stemWordVertical;
        private IEnumerable<string> crossWords;

        public bool Bonus { get; }

        public PotentialMove(Board board, IList<Placement> placements)
        {
            this.board = board;
            Placements = placements;
            Words = WordScores();
            TotalScore = Words.Sum(w => w.Score);
            if (placements.Count == 7)
            {
                TotalScore += 50;
                Bonus = true;
            }
        }

        public IList<Placement> Placements { get; }

        public IList<WordScore> WordScores()
        {
            if (Placements.Count == 0)
                return WordScoresInitial();
            var words = new List<WordScore>();
            var square = Placements[0].Square;
            var direction = MoveDirection();
            var word = GetWordFormedScore(square, direction);
            if (word != null)
            {
                words.Add(word);
                SetStemWord(word.Word, direction);
            }
            words.AddRange(GetCrossWordScores(direction));
            return words;
        }

        private void SetStemWord(string word, Direction direction)
        {
            word = word.ToUpperInvariant();
            switch (direction)
            {
                case Direction.Horizontal:
                    stemWordHorizontal = word;
                    break;
                case Direction.Vertical:
                    stemWordVertical = word;
                    break;
            }
        }

        private string GetStemWord(Direction direction)
        {
            switch (direction)
            {
                case Direction.Horizontal: return stemWordHorizontal;
                case Direction.Vertical: return stemWordVertical;
                default: throw new ArgumentException("Invalid direction", nameof(direction));
            }
        }

        private IList<WordScore> WordScoresInitial()
        {
            var words = new List<WordScore>();
            var square = Placements[0].Square;
            var word = GetWordFormedScore(square, Direction.Horizontal);
            if (word != null)
            {
                words.Add(word);
                SetStemWord(word.Word, Direction.Horizontal);
            }
            word = GetWordFormedScore(square, Direction.Vertical);
            if (word != null)
            {
                words.Add(word);
                SetStemWord(word.Word, Direction.Vertical);
            }
            return words;
        }

        public bool IsValid(IWordDatabase wordlist)
        {
            return Placements.Any(p => board.IsPlayable(p.Square))
                && Words.All(w => wordlist.Contains(w.Word.ToUpperInvariant()));
        }

        private Direction MoveDirection()
        {
            Direction direction;
            if (Placements.Count == 1)
            {
                direction = Direction.Horizontal;
            }
            else if ((Placements[1].Square - Placements[0].Square) < 15)
            {
                direction = Direction.Horizontal;
            }
            else
            {
                direction = Direction.Vertical;
            }
            return direction;
        }

        public bool IsValidStem(Direction direction, IWordDatabase wordlist)
        {
            var stemWord = GetStemWord(direction);
            return (stemWord == null || wordlist.ContainsPrefix(stemWord))
                && crossWords.All(wordlist.Contains);
        }

        private static Direction OtherDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Horizontal: return Direction.Vertical;
                case Direction.Vertical: return Direction.Horizontal;
                default: throw new ArgumentException("Invalid direction", nameof(direction));
            }
        }

        private IEnumerable<WordScore> GetCrossWordScores(Direction direction)
        {
            var words = new List<WordScore>();
            var crossDirection = OtherDirection(direction);
            foreach (var placement in Placements)
            {
                var word = GetWordFormedScore(placement.Square, crossDirection);
                if (word != null)
                    words.Add(word);
            }
            crossWords = words.Select(w => w.Word.ToUpperInvariant());
            return words;
        }

        private WordScore GetWordFormedScore(int square, Direction direction)
        {
            // start from the first placed tile and move backward in direction while there are used tiles
            var start = MoveToStartOfWord(square, direction);
            return GetFullWordScore(start, direction);
        }

        private int MoveToStartOfWord(int square, Direction direction)
        {
            while (true)
            {
                var prevSquare = GameRules.PreviousSquare(square, direction);
                if (prevSquare == null || board.IsOpen(prevSquare.Value))
                {
                    return square;
                }
                square = prevSquare.Value;
            }
        }

        public WordScore GetFullWordScore(int start, Direction direction)
        {
            int wordMultiplier = 1;
            int score = 0;
            var sb = new StringBuilder();
            int? square = start;
            while (square != null)
            {
                var letter = board[square.Value];
                if (char.IsLetter(letter))
                {
                    score += GameRules.TileScore(letter);
                }
                else
                {
                    var placement = Placements.FirstOrDefault(p => p.Square == square);
                    if (placement == null) break;
                    wordMultiplier *= WordMultiplier(letter);
                    var letterMultiplier = LetterMultiplier(letter);
                    letter = placement.Tile;
                    score += letterMultiplier * GameRules.TileScore(placement.Tile);
                }
                sb.Append(letter);
                square = GameRules.NextSquare(square.Value, direction);
            }
            if (sb.Length > 1)
                return new WordScore(sb.ToString(), score * wordMultiplier);
            return null;
        }

        private int WordMultiplier(char c)
        {
            if (c == '3') return 3;
            if (c == '2') return 2;
            return 1;
        }

        private int LetterMultiplier(char c)
        {
            if (c == '*') return 3;
            if (c == '+') return 2;
            return 1;
        }

        public PotentialMove Extend(char tile, Direction direction)
        {
            // Find the next open tile in direction, if any, and return a potential move that has the tile placed there
            var lastSquare = Placements[Placements.Count - 1].Square;
            if (direction == Direction.Horizontal)
            {
                var end = lastSquare - (lastSquare % 15) + 15;
                for (int i = lastSquare + 1; i < end; i++)
                {
                    if (board.IsOpen(i))
                    {
                        return Extend(tile, i);
                    }
                }
            }
            else if (direction == Direction.Vertical)
            {
                for (int i = lastSquare + 15; i < 225; i += 15)
                {
                    if (board.IsOpen(i))
                    {
                        return Extend(tile, i);
                    }
                }
            }
            return null;
        }

        public PotentialMove Extend(char tile, int square)
        {
            var placements = new Placement[Placements.Count + 1];
            Placements.CopyTo(placements, 0);
            placements[Placements.Count] = new Placement(tile, square);
            return new PotentialMove(board, placements);
        }

        private static string SquareToCoords(int square)
        {
            var row = Math.DivRem(square, 15, out var col);
            return string.Format("{0}{1}", "ABCDEFGHIJKLMNO"[col], row + 1);
        }

        public override string ToString()
        {
            var scores = WordScores();
            var sb = new StringBuilder();
            sb.AppendLine("----[ " + TotalScore + " ]----");
            foreach (var placement in Placements)
            {
                sb.Append(SquareToCoords(placement.Square) + ":" + placement.Tile + ", ");
            }
            sb.AppendLine();
            foreach (var score in scores)
            {
                sb.AppendLine(score.Word + ": " + score.Score);
            }
            if (Bonus)
            {
                sb.AppendLine("Bingo: 50");
            }
            return sb.ToString();
        }
    }
}
