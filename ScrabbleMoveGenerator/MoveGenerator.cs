﻿using System.Collections.Generic;

namespace ScrabbleMoveGenerator
{

    public class MoveGenerator
    {
        private readonly Board board;
        private readonly IWordDatabase wordlist;

        public MoveGenerator(Board board, IWordDatabase wordlist)
        {
            this.board = board;
            this.wordlist = wordlist;
        }

        /// <summary>
        /// Find all the possible plays, per the word list, in order from highest to lowest total score.
        /// </summary>
        /// <param name="tiles">The tiles in the current player's hand.</param>
        /// <returns></returns>
        public IList<PotentialMove> FindMoves(IList<char> tiles)
        {
            var moves = new List<PotentialMove>();
            for (int square = 0; square < 225; square++)
            {
                if (IsPotentialStartingSquare(square, tiles.Count))
                {
                    foreach (var (tile, rest) in EachWithRest(tiles))
                    {
                        TryBeginningAtSquare(moves, square, tile, rest);
                    }
                }
            }
            moves.Sort((a, b) => b.TotalScore.CompareTo(a.TotalScore));
            return moves;
        }

        private void TryBeginningAtSquare(List<PotentialMove> moves, int square, char tile, IList<char> rest)
        {
            // try placing tile at square
            var potentialMove = new PotentialMove(board, Placement.BeginList(tile, square));
            if (potentialMove.IsValid(wordlist))
            {
                moves.Add(potentialMove);
            }
            if (potentialMove.IsValidStem(Direction.Horizontal, wordlist))
            {
                AccumulateRest(moves, potentialMove, rest, Direction.Horizontal);
            }
            if (potentialMove.IsValidStem(Direction.Vertical, wordlist))
            {
                AccumulateRest(moves, potentialMove, rest, Direction.Vertical);
            }
        }

        private bool IsPotentialStartingSquare(int square, int tiles)
        {
            return board.IsOpen(square) && PlayableSquareWithin(tiles, square);
        }

        private bool PlayableSquareWithin(int count, int start)
        {
            int? hsquare = start;
            int? vsquare = start;
            for (int i = 0; i < count && (hsquare.HasValue || vsquare.HasValue); i++)
            {
                if (hsquare.HasValue)
                {
                    if (board.IsPlayable(hsquare.Value))
                        return true;
                    hsquare = GameRules.NextSquare(hsquare.Value, Direction.Horizontal);
                }
                if (vsquare.HasValue)
                {
                    if (board.IsPlayable(vsquare.Value))
                        return true;
                    vsquare = GameRules.NextSquare(vsquare.Value, Direction.Vertical);
                }
            }
            return false;
        }

        private void AccumulateRest(ICollection<PotentialMove> moves, PotentialMove move, IList<char> remainingTiles, Direction direction)
        {
            foreach (var (tile, rest) in EachWithRest(remainingTiles))
            {
                var potentialMove = move.Extend(tile, direction);
                if (potentialMove == null)
                    continue;
                if (potentialMove.IsValid(wordlist))
                {
                    moves.Add(potentialMove);
                }
                if (potentialMove.IsValidStem(direction, wordlist))
                {
                    AccumulateRest(moves, potentialMove, rest, direction);
                }
            }
        }

        private IEnumerable<(char, IList<char>)> EachWithRest(IList<char> source)
        {
            int len = source.Count;
            var tried = new List<char>();
            for (int i = 0; i < len; i++)
            {
                var c = source[i];
                if (tried.Contains(c)) continue;
                tried.Add(c);
                IList<char> rest = new List<char>(len - 1);
                for (int j = 0; j < len; j++)
                {
                    if (i != j)
                    {
                        rest.Add(source[j]);
                    }
                }
                if (c == '?' || c == '_')
                    foreach (var optc in "abcdefghijklmnopqrstuvwxyz")
                        yield return (optc, rest);
                else
                    yield return (c, rest);
            }
        }
    }
}
