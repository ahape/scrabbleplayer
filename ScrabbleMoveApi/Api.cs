﻿using System;
using System.Linq;
using ScrabbleMoveGenerator;
using System.Collections.Immutable;
//using ScrabblePlayer;

namespace ScrabbleMoveApi
{
    public static class Api
    {
        static IndexedWordList defaultWordList = new NaspaWordList();
        
        /// <summary>Finds the best word for a particular game state</summary>
        public static PotentialMove FindBestWord(
            int teams, 
            char[] encBoard, 
            char[] bag, 
            char[] rack, 
            int teamTurn)
        {
            var board = new Board(encBoard);
            var players = ImmutableList.Create(Enumerable.Range(0, teams)
                .Select(x => new Player("Team " + (x + 1), 0, "")) 
                .ToArray());
            var state = new GameState(board, players, ImmutableList.Create(bag), teamTurn - 1);
            var gen = new MoveGenerator(state.Board, defaultWordList);
            var moves = gen.FindMoves(rack);

            if (moves.Any())
                return moves[0];

            return null;
        }

        public static bool IsWordReal(string proposedWord)
        {
            if (string.IsNullOrEmpty(proposedWord))
                return false;

            return defaultWordList.Contains(proposedWord.Trim().ToUpper());
        }
    }
}
