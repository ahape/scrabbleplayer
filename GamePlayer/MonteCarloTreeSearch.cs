﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GamePlayerNs
{
    public interface IState
    {
        IState Expand();
        int Player { get; }
    }

    public class MonteCarloTreeSearch<TAction>
    {
        private class Node
        {
            public Node(IState state)
            {
                this.state = state;
            }
            public readonly IState state;
            public int wins;
            public int plays;
            public Node parent;
            public List<Node> children;
        }

        public TAction Search(IState state, CancellationToken token)
        {
            var tree = new Node(state);
            while (!token.IsCancellationRequested)
            {
                var leaf = Select(tree);
                var child = Expand(leaf);
                var result = Simulate(child);
                BackPropogate(result, child);
            }
        }

        private Node Select(Node node)
        {
            while (node.children != null)
            {
                node = node.children.OrderBy(Ucb1).First();
            }
            return node;
        }

        private double Ucb1(Node node)
        {

        }

        private Node Expand(Node node)
        {
            var childState = node.state.Expand();
            var childNode = new Node(childState);
        }

        private int Simulate(Node node)
        {

        }

        private void BackPropogate(int result, Node child)
        {
            while (child != null)
            {
                child.plays++;
                if (result == child.player)
                    child.wins++;
            }
        }
    }
}
