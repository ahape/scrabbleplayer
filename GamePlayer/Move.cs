﻿namespace GamePlayerNs
{
    public class Move<TState, TAction>
    {
        public Move(TState state, TAction action)
        {
            State = state;
            Action = action;
        }
        public TState State { get; }
        public TAction Action { get; }

        public override string ToString()
        {
            return "Move: " + Action + "\nNew State: " + State + "\n";
        }
    }

    public static class Move
    {
        public static Move<TState, TAction> Create<TState, TAction>(TState state, TAction action)
        {
            return new Move<TState, TAction>(state, action);
        }
    }
}
