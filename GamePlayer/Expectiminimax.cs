﻿using System;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;

namespace GamePlayerNs
{

    public class Expectiminimax<TState, TAction>
    {
        private readonly IChanceGameRules<TState, TAction> game;
        private bool usedHeuristics;
        private CancellationToken cancellationToken;
        private int depthReached;
        private int statesExamined;

        public Expectiminimax(IChanceGameRules<TState, TAction> game)
        {
            this.game = game;
        }

        public Move<TState, TAction> Search(TState state, CancellationToken cancellationToken)
        {

            this.cancellationToken = cancellationToken;
            depthReached = 1;
            statesExamined = 0;
            Move<TState, TAction> best = null;
            while (!cancellationToken.IsCancellationRequested)
            {
                usedHeuristics = false;
                try
                {
                    best = SearchToDepth(state, depthReached);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                if (!usedHeuristics) break;
                depthReached++;
            }
            return best;
        }

        public string GetStats()
        {
            return $"Examined {statesExamined} states to a depth of {depthReached}";
        }

        private Move<TState, TAction> SearchToDepth(TState state, int depth)
        {
            var playerToMove = game.PlayerToMove(state);
            var bestValue = double.NegativeInfinity;
            Move<TState, TAction> bestMove = null;
            var bestLock = new object();
            var opts = new ParallelOptions { CancellationToken = cancellationToken };
            Parallel.ForEach(game.Expand(state), opts, move =>
            {
                var moveValues = MaxValue(move.State, depth - 1);
                var moveValue = moveValues[playerToMove];
                lock (bestLock)
                {
                    if (moveValue > bestValue)
                    {
                        bestValue = moveValue;
                        bestMove = move;
                    }
                }
            });
            return bestMove;
        }

        private Vector<double> AverageValue(TState state, int depth)
        {
            Vector<double> totalValue = Vector<double>.Zero;
            foreach (var chance in game.ExpandChances(state))
            {
                var moveValues = MaxValue(chance.State, depth);
                totalValue += chance.Probability * moveValues;
            }
            return totalValue;
        }

        private Vector<double> MaxValue(TState state, int depth)
        {
            Interlocked.Increment(ref statesExamined);
            if (game.IsTerminal(state))
            {
                return AsVector(game.Utility(state));
            }
            if (depth == 0)
            {
                usedHeuristics = true;
                return AsVector(game.Heuristic(state));
            }
            cancellationToken.ThrowIfCancellationRequested();
            var playerToMove = game.PlayerToMove(state);
            var bestValue = double.NegativeInfinity;
            Vector<double> bestValues = default;
            foreach (var move in game.Expand(state))
            {
                var averageValues = AverageValue(move.State, depth - 1);
                var moveValue = averageValues[playerToMove];
                if (moveValue > bestValue)
                {
                    bestValue = moveValue;
                    bestValues = averageValues;
                }
            }
            return bestValues;
        }

        private static Vector<double> AsVector(double[] source)
        {
            if (source.Length > Vector<double>.Count)
                throw new InvalidOperationException("array can not be represented as vector");
            if (source.Length < Vector<double>.Count)
            {
                var vectorSource = new double[Vector<double>.Count];
                Array.Copy(source, vectorSource, source.Length);
                return new Vector<double>(vectorSource);
            }
            return new Vector<double>(source);
        }
    }

    public static class Expectiminimax
    {
        public static Expectiminimax<TState, TAction> Create<TState, TAction>(IChanceGameRules<TState, TAction> game)
        {
            return new Expectiminimax<TState, TAction>(game);
        }
    }
}
