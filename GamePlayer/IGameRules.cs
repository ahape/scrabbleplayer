﻿using System.Collections.Generic;

namespace GamePlayerNs
{
    public interface IGameRules<TState, TAction>
    {
        int PlayerToMove(TState state);
        bool IsTerminal(TState state);
        double[] Utility(TState state);
        double[] Heuristic(TState state);
        IEnumerable<Move<TState, TAction>> Expand(TState state);
    }

    public interface IChanceGameRules<TState, TAction> : IGameRules<TState, TAction>
    {
        IEnumerable<Chance<TState>> ExpandChances(TState state);
    }

    public class Chance<TState>
    {
        public Chance(TState state, Ratio probability)
        {
            State = state;
            Probability = probability;
        }
        public Chance(TState state, int numerator, int denominator)
            : this(state, new Ratio(numerator, denominator))
        { }
        public TState State { get; }
        public Ratio Probability { get; }
    }

    public struct Ratio
    {
        public Ratio(int numerator, int denominator)
        {
            Numerator = numerator;
            Denominator = denominator;
        }
        public int Numerator { get; }
        public int Denominator { get; }
        public double ToDouble()
        {
            return (double)Numerator / Denominator;
        }
        public static implicit operator double(Ratio ratio)
        {
            return ratio.ToDouble();
        }
        public override string ToString()
        {
            return Numerator + "/" + Denominator;
        }
        public static readonly Ratio One = new Ratio(1, 1);
    }
}
