﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GamePlayerNs
{

    public class Minimax<TState, TAction>
    {
        private readonly IGameRules<TState, TAction> game;
        private bool usedHeuristics;
        private CancellationToken cancellationToken;
        private int maxDepth;
        private int statesExamined;

        public Minimax(IGameRules<TState, TAction> game)
        {
            this.game = game;
        }

        public Move<TState, TAction> Search(TState state, CancellationToken cancellationToken)
        {

            this.cancellationToken = cancellationToken;
            maxDepth = 1;
            statesExamined = 0;
            Move<TState, TAction> best = null;
            while (!cancellationToken.IsCancellationRequested)
            {
                usedHeuristics = false;
                try
                {
                    best = SearchToDepth(state, maxDepth);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                if (!usedHeuristics) break;
                maxDepth++;
            }
            return best;
        }

        public string GetStats()
        {
            return $"Examined {statesExamined} states to a depth of {maxDepth}";
        }

        private Move<TState, TAction> SearchToDepth(TState state, int depth)
        {
            var (_, move) = MaxValue(state, depth);
            return move;
        }

        private (double[], Move<TState, TAction>) MaxValue(TState state, int depth)
        {
            Interlocked.Increment(ref statesExamined);
            if (game.IsTerminal(state))
            {
                return (game.Utility(state), default);
            }
            if (depth == 0)
            {
                usedHeuristics = true;
                return (game.Heuristic(state), default);
            }
            cancellationToken.ThrowIfCancellationRequested();
            var playerToMove = game.PlayerToMove(state);
            var bestValue = double.NegativeInfinity;
            double[] bestValues = null;
            Move<TState, TAction> bestMove = null;
            if (depth > 3)
            {
                var bestLock = new object();
                Parallel.ForEach(game.Expand(state), new ParallelOptions { CancellationToken = cancellationToken }, move =>
                {
                    var (moveValues, possibleMove) = MaxValue(move.State, depth - 1);
                    var moveValue = moveValues[playerToMove];
                    lock (bestLock)
                    {
                        if (moveValue > bestValue)
                        {
                            bestValue = moveValue;
                            bestValues = moveValues;
                            bestMove = move;
                        }
                    }
                });
            }
            else
            {
                foreach (var move in game.Expand(state))
                {
                    var (moveValues, possibleMove) = MaxValue(move.State, depth - 1);
                    var moveValue = moveValues[playerToMove];
                    if (moveValue > bestValue)
                    {
                        bestValue = moveValue;
                        bestValues = moveValues;
                        bestMove = move;
                    }
                }
            }
            return (bestValues, bestMove);
        }
    }

    public static class Minimax
    {
        public static Minimax<TState, TAction> Create<TState, TAction>(IGameRules<TState, TAction> game)
        {
            return new Minimax<TState, TAction>(game);
        }
    }
}
